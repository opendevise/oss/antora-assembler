'use strict'

const filterComponentVersions = require('./filter-component-versions')
const produceAggregateDocument = require('./produce-aggregate-document')
const selectMutableAttributes = require('./select-mutable-attributes')

function produceAggregateDocuments (loadAsciiDoc, contentCatalog, assemblerConfig) {
  const { insertStartPage, rootLevel, sectionMergeStrategy, asciidoc: assemblerAsciiDocConfig } = assemblerConfig
  const assemblerAsciiDocAttributes = Object.assign({}, assemblerAsciiDocConfig.attributes)
  const { doctype, 'source-highlighter': sourceHighlighter } = assemblerAsciiDocAttributes
  delete assemblerAsciiDocAttributes.doctype
  delete assemblerAsciiDocAttributes['source-highlighter']
  return filterComponentVersions(contentCatalog.getComponents(), assemblerConfig.componentVersions).reduce(
    (accum, componentVersion) => {
      const { name: componentName, version, title, navigation } = componentVersion
      if (!navigation) return accum
      const componentVersionAsciiDocConfig = getAsciiDocConfigWithAsciidoctorReducerExtension(componentVersion)
      const mergedAsciiDocConfig = Object.assign({}, componentVersionAsciiDocConfig, {
        attributes: Object.assign({}, componentVersionAsciiDocConfig.attributes, assemblerAsciiDocAttributes),
      })
      const rootEntry = { content: title }
      const startPage = contentCatalog.getComponentVersionStartPage(componentName, version)
      let startPageUrl
      if (startPage && insertStartPage) {
        if (includedInNav(navigation, (startPageUrl = startPage.pub.url))) {
          startPageUrl = undefined
        } else {
          Object.assign(rootEntry, { url: startPageUrl, urlType: 'internal' })
        }
      }
      // Q: should we use an artificial page instead or as fallback?
      const mutableAttributes = startPage
        ? selectMutableAttributes(loadAsciiDoc, contentCatalog, startPage, mergedAsciiDocConfig)
        : {}
      delete mutableAttributes.doctype
      accum = accum.concat(
        prepareOutlines(navigation, rootEntry, rootLevel).map((outline) =>
          produceAggregateDocument(
            loadAsciiDoc,
            contentCatalog,
            componentVersion,
            outline,
            doctype,
            contentCatalog.getPages((page) => page.out),
            mergedAsciiDocConfig,
            mutableAttributes,
            sectionMergeStrategy
          )
        )
      )
      mergedAsciiDocConfig.attributes.doctype = doctype
      sourceHighlighter
        ? (mergedAsciiDocConfig.attributes['source-highlighter'] = sourceHighlighter)
        : delete mergedAsciiDocConfig.attributes['source-highlighter']
      return accum
    },
    []
  )
}

function getAsciiDocConfigWithAsciidoctorReducerExtension (componentVersion) {
  const asciidoctorReducerExtension = require('./asciidoctor/reducer-extension') // NOTE: must be required lazily
  const asciidocConfig = componentVersion.asciidoc
  const extensions = asciidocConfig.extensions || []
  if (extensions.length) {
    return Object.assign({}, asciidocConfig, {
      extensions: extensions.reduce(
        (accum, candidate) => {
          if (candidate !== asciidoctorReducerExtension) accum.push(candidate)
          return accum
        },
        [asciidoctorReducerExtension]
      ),
      sourcemap: true,
    })
  }
  return Object.assign({}, asciidocConfig, { extensions: [asciidoctorReducerExtension], sourcemap: true })
}

function includedInNav (items, url) {
  return items.find((it) => it.url === url || includedInNav(it.items || [], url))
}

function prepareOutlines (navigation, rootEntry, rootLevel) {
  if (rootLevel === 0 || navigation.length === 1) {
    const navBranch =
      navigation.length === 1
        ? navigation[0]
        : { items: navigation.reduce((navTree, it) => navTree.concat(it.content ? it : it.items), []) }
    return [Object.assign(rootEntry, navBranch)]
  }
  return navigation.reduce((navTree, it) => navTree.concat(it.content ? it : it.items), [rootEntry])
}

module.exports = produceAggregateDocuments
