'use strict'

const fs = require('fs')
const LazyReadable = require('./lazy-readable')
const { spawn } = require('child_process')

const IS_WIN = process.platform === 'win32'
const DBL_QUOTE_RX = /"/g

const shellEscape = IS_WIN
  ? (val) => {
      if (val.charAt() === '-') {
        return val
      } else if (~val.indexOf('"')) {
        return ~val.indexOf(' ') ? `"${val.replace(DBL_QUOTE_RX, '"""')}"` : val.replace(DBL_QUOTE_RX, '""')
      } else if (~val.indexOf(' ')) {
        return `"${val}"`
      }
      return val
    }
  : (val) => val

async function runCommand (cmd, argv = [], opts = {}) {
  if (!cmd) throw new TypeError('Command not specified')
  const cmdv = cmd.split(' ').map(shellEscape)
  const { input, output, implicitStdin, ...spawnOpts } = opts
  if (input) input instanceof Buffer ? implicitStdin || argv.push('-') : argv.push(input)
  if (IS_WIN) Object.assign(spawnOpts, { shell: true, windowsHide: true })
  return new Promise((resolve, reject) => {
    const stdout = []
    const stderr = []
    const ps = spawn(cmdv[0], [...cmdv.slice(1), ...argv.map(shellEscape)], spawnOpts)
    ps.on('close', (code) => {
      if (code === 0) {
        if (stderr.length) process.stderr.write(stderr.join(''))
        resolve(output ? new LazyReadable(() => fs.createReadStream(output)) : Buffer.from(stdout.join('')))
      } else {
        let msg = `Command failed: ${ps.spawnargs.join(' ')}`
        if (stderr.length) msg += '\n' + stderr.join('')
        reject(new Error(msg))
      }
    })
    ps.on('error', (err) => reject(err.code === 'ENOENT' ? new Error(`Command not found: ${cmdv.join(' ')}`) : err))
    ps.stdout.on('data', (data) => (output ? process.stdout.write(data) : stdout.push(data)))
    ps.stderr.on('data', (data) => stderr.push(data))
    if (input instanceof Buffer) {
      try {
        ps.stdin.on('error', () => undefined)
        ps.stdin.end(input)
      } catch (err) {
        if (!ps.stdin.writableEnded) ps.stdin.end()
        reject(err)
      }
    } else {
      ps.stdin.end()
    }
  })
}

module.exports = runCommand
