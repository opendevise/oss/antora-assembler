'use strict'

const Opal = global.Opal
const Asciidoctor = Opal.Asciidoctor
const $Reducer = function $Reducer () {}

const DocumentExt = (() => {
  const parentScope = Opal.module(Asciidoctor, 'Reducer', $Reducer)
  const scope = Opal.module(parentScope, 'DocumentExt', function $DocumentExt () {})

  Opal.defn(scope, '$save_attributes', function saveAttributes () {
    this.attributes_defined_in_header = this.attributes_modified.$to_a().reduce((accum, name) => {
      accum[name] = this.getAttribute(name)
      return accum
    }, {})
    return Opal.send(this, Opal.find_super_dispatcher(this, 'save_attributes', saveAttributes), [])
  })

  return scope
})()

const ConditionalDirectiveTracker = (() => {
  const parentScope = Opal.module(Asciidoctor, 'Reducer', $Reducer)
  const scope = Opal.module(parentScope, 'ConditionalDirectiveTracker', function $ConditionalDirectiveTracker () {})

  Opal.defn(
    scope,
    '$preprocess_conditional_directive',
    function preprocessConditionalDirective (keyword, target, delimiter, text) {
      const skipActive = this.skipping
      const depth = this.conditional_stack.length
      let directiveLineno = this.lineno
      const result = Opal.send(
        this,
        Opal.find_super_dispatcher(this, 'preprocess_conditional_directive', preprocessConditionalDirective),
        [keyword, target, delimiter, text]
      )
      if (this.skipping && skipActive) return result
      const currIncReplacement = this.includeReplacements.$current()
      const drop = currIncReplacement.drop || (currIncReplacement.drop = [])
      directiveLineno -= currIncReplacement.offset || 0
      const depthChange = this.conditional_stack.length - depth
      if (depthChange < 0) {
        if (skipActive) {
          for (let n = drop.pop(); n <= directiveLineno; n++) drop.push(n)
        } else {
          drop.push(directiveLineno)
        }
      } else if (depthChange > 0 || directiveLineno === this.lineno) {
        drop.push(directiveLineno)
      } else {
        drop.push([directiveLineno, text])
      }
      return result
    }
  )

  return scope
})()

const CurrentPosition = (() => {
  const parentScope = Opal.module(Opal.Asciidoctor, 'Reducer', $Reducer)
  const scope = Opal.module(parentScope, 'CurrentPosition', function $CurrentPosition () {})

  Opal.defn(Opal.get_singleton_class(scope), '$extended', function $extended (instance) {
    instance.$to_end()
  })

  Opal.defn(scope, '$current', function $current () {
    return this[this.pointer]
  })

  Opal.defn(scope, '$to_end', function $toEnd () {
    this.pointer = this.length - 1
  })

  Opal.defn(scope, '$up', function $up () {
    this.pointer = this.$current().into
  })

  return scope
})()

const IncludeDirectiveTracker = (() => {
  const parentScope = Opal.module(Opal.Asciidoctor, 'Reducer', $Reducer)
  const scope = Opal.module(parentScope, 'IncludeDirectiveTracker', function $IncludeDirectiveTracker () {})

  Opal.defn(Opal.get_singleton_class(scope), '$extended', function $extended (instance) {
    instance.includeReplacements = [{}].$extend(CurrentPosition)
    instance.$$reducer = {}
  })

  Opal.defn(scope, '$preprocess_include_directive', function preprocessIncludeDirective (target, attrlist) {
    this.$$reducer.includeDirectiveLine = `include::${target}[${attrlist}]`
    this.$$reducer.includePushed = false
    const directiveLineno = this.lineno // we're currently on the include line, which is 1-based
    const result = Opal.send(
      this,
      Opal.find_super_dispatcher(this, 'preprocess_include_directive', preprocessIncludeDirective),
      [target, attrlist]
    )
    if (!this.$$reducer.includePushed) {
      let ln = this.peekLine(true)
      let unresolved
      if (
        ln &&
        ln.charAt(ln.length - 1) === ']' &&
        !(unresolved = ln.startsWith('Unresolved directive in ')) &&
        directiveLineno === this.lineno &&
        (unresolved = ln.startsWith('link:'))
      ) {
        ln = `${ln.slice(0, ln.length - 1)}role=include]`
      }
      pushIncludeReplacement.call(this, directiveLineno, unresolved ? [ln] : [], 0, unresolved)
    }
    this.$$reducer = {}
    return result
  })

  Opal.defn(scope, '$push_include', function pushInclude (data, file, path, lineno, attrs) {
    this.$$reducer.includePushed = true
    const directiveLineno = this.lineno - 1 // we're below the include line, which is 1-based
    const prevIncDepth = this.include_stack.length
    const result = Opal.send(this, Opal.find_super_dispatcher(this, 'push_include', pushInclude), [
      data,
      file,
      path,
      lineno,
      attrs,
    ])
    pushIncludeReplacement.call(
      this,
      directiveLineno,
      this.include_stack.length > prevIncDepth ? this.$lines() : [],
      lineno > 1 ? lineno - 1 : 0
    )
    return result
  })

  Opal.defn(scope, '$pop_include', function popInclude () {
    if (!this.$$reducer.includePushed) this.includeReplacements.$up()
    return Opal.send(this, Opal.find_super_dispatcher(this, 'pop_include', popInclude), [])
  })

  function pushIncludeReplacement (lineno, lines, offset, unresolved) {
    const incReplacements = this.includeReplacements
    const into = incReplacements.pointer
    const line = this.$$reducer.includeDirectiveLine
    incReplacements.push({ into, lineno: lineno - (incReplacements.$current().offset || 0), line, lines, offset })
    if (!unresolved && lines.length) incReplacements.$to_end()
  }

  return scope
})()

function preprocessor () {
  this.process((doc, reader) =>
    doc.getOptions().preserve_conditionals
      ? reader.$extend(IncludeDirectiveTracker)
      : reader.$extend(ConditionalDirectiveTracker, IncludeDirectiveTracker)
  )
}

function treeProcessor () {
  this.process((doc) => {
    const incReplacements = doc.reader.includeReplacements
    if (incReplacements.length > 1 || (incReplacements[0].drop || []).length) {
      const sourceLines = doc.getSourceLines()
      incReplacements[0].lines = sourceLines.slice()
      incReplacements
        .slice()
        .reverse()
        .forEach(({ into, lineno, lines, line, drop }) => {
          let targetLines, idx
          if (into != null) {
            targetLines = incReplacements[into].lines
            // adds assurance that we're replacing the correct line
            if (targetLines[(idx = lineno - 1)] !== line) return
          }
          if ((drop || []).length) {
            drop
              .slice()
              .reverse()
              .forEach((dropIt) => {
                Array.isArray(dropIt) ? (lines[dropIt[0] - 1] = dropIt[1]) : lines.splice(dropIt - 1, 1)
              })
          }
          if (targetLines) targetLines[idx] = lines
        })
      const reducedSourceLines = flattenDeep(incReplacements[0].lines)
      if (doc.getSourcemap()) {
        const logger = Asciidoctor.LoggerManager.getLogger()
        const opts = Object.assign(doc.getOptions(), { logger: undefined, parse: false, reduced: true })
        if (opts.extension_registry) {
          opts.extension_registry = Asciidoctor.Extensions.Registry.$new(opts.extension_registry.groups)
        }
        const includes = doc.getCatalog().includes
        doc = Asciidoctor.load(reducedSourceLines, opts)
        doc.catalog.$send('[]=', 'includes', includes)
        doc.parse()
        Asciidoctor.LoggerManager.setLogger(logger)
      } else {
        while (reducedSourceLines[reducedSourceLines.length - 1] === '') reducedSourceLines.pop()
        sourceLines.splice(0, sourceLines.length, ...reducedSourceLines)
      }
    }
    return doc
  })
}

function flattenDeep (array, accum = []) {
  const len = array.length
  for (let i = 0, it; i < len; i++) Array.isArray((it = array[i])) ? flattenDeep(it, accum) : accum.push(it)
  return accum
}

function toProc (fn) {
  return Object.defineProperty(fn, '$$arity', { value: fn.length })
}

module.exports.register = (registry) => {
  const extGroup = toProc(function () {
    const doc = this.document
    doc.$extend(DocumentExt)
    if (doc.getOptions().reduced) return
    this.preprocessor(preprocessor)
    this.treeProcessor(treeProcessor)
  })
  registry.groups.$send('[]=', 'reducer', extGroup)
}
