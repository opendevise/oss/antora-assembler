'use strict'

const expandPath = require('@antora/expand-path-helper')
const { promises: fsp } = require('fs')
const os = require('os')
const yaml = require('js-yaml')

function loadConfig (playbook, configSource = './antora-assembler.yml') {
  return (
    configSource.constructor === Object
      ? Promise.resolve(configSource)
      : fsp
        .access((configSource = expandPath(configSource, { dot: playbook.dir })))
        .then(
          () => true,
          () => false
        )
        .then((exists) =>
          exists ? fsp.readFile(configSource).then((data) => camelCaseKeys(yaml.load(data), ['asciidoc'])) : {}
        )
  ).then((config) => {
    if (config.enabled === false) return undefined
    let asciidocAttrs
    if (!config.asciidoc) {
      config.asciidoc = { attributes: (asciidocAttrs = {}) }
    } else if (!(asciidocAttrs = config.asciidoc.attributes)) {
      config.asciidoc.attributes = asciidocAttrs = {}
    }
    if (!('doctype' in asciidocAttrs)) asciidocAttrs.doctype = 'book'
    asciidocAttrs.revdate = new Date().toISOString().split('T')[0]
    asciidocAttrs['page-partial'] = null
    if (config.componentVersions == null) {
      config.componentVersions = ['*']
    } else if (typeof config.componentVersions === 'string') {
      config.componentVersions = config.componentVersions.split(', ')
    }
    if (!('rootLevel' in config)) config.rootLevel = 0
    if (!('insertStartPage' in config)) config.insertStartPage = true
    if (['discrete', 'fuse', 'enclose'].indexOf(config.sectionMergeStrategy) < 0) {
      config.sectionMergeStrategy = 'discrete'
    }
    const build = config.build || (config.build = {})
    if (build.dir === '$' + '{playbook.output.dir}') {
      //build.dir = playbook.output.dir
      throw new Error('Not implemented')
    } else {
      build.dir = expandPath(build.dir || './build/assembler', { dot: playbook.dir })
    }
    build.cwd = playbook.dir // use playbook.dir the purpose of finding and loading require scripts
    if (!('clean' in build) && 'output' in playbook) build.clean = playbook.output.clean
    if (!('publish' in build)) build.publish = true
    if (!build.processLimit) {
      build.processLimit = 'processLimit' in build ? Infinity : Math.round(os.cpus().length * 0.5)
    }
    return config
  })
}

function camelCaseKeys (o, stopPaths = [], p) {
  if (Array.isArray(o)) return o.map((it) => camelCaseKeys(it, stopPaths, p))
  if (o == null || o.constructor !== Object) return o
  const pathPrefix = p ? p + '.' : ''
  const accum = {}
  for (const [k, v] of Object.entries(o)) {
    const camelKey = k.charAt() + k.substr(1).replace(/_([a-z])/g, (_, l) => l.toUpperCase())
    accum[camelKey] = ~stopPaths.indexOf(pathPrefix + camelKey) ? v : camelCaseKeys(v, stopPaths, pathPrefix + camelKey)
  }
  return accum
}

module.exports = loadConfig
